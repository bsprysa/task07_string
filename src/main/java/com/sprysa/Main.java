package com.sprysa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {

  public static String getText() {
    String text = "Космоплан Virgin Galactic досяг рекордних висот. Літальний апарат створений для "
        + "суборбітальних польотів. В майбутньому на ньому планують відправляти людей в космос. Космічний"
        + " апарат SpaceShipTwo компанії Virgin Galactic здійснив четвертий тестовий політ і встановив "
        + "світовий рекорд. Він піднявся на висоту 82,7 кілометра зі швидкістю 2,9 Мах. Про це "
        + "повідомляється на Twitter-сторінці компанії. Компанія Virgin Galactic оголосила про те, що "
        + "SpaceShipTwo вийшов \"в космічний простір\". Водночас Міжнародна авіафедерація зараз вважає, "
        + "що межа космосу пролягає на висоті 100 кілометрів. Відомо, що Virgin Galactic, що належить "
        + "Річарду Бренсону, має намір здійснювати польоти з пасажирами на регулярній основі.";
    return text;
  }

  public static void task1() {
    String[] sentenses = getText().split("(\\.|\\?|\\!|[.]{3})(\\s)");
    List<List<String>> words = new ArrayList<>();
    for (int i = 0; i < sentenses.length; i++) {
      words.add(Arrays.asList(sentenses[i].replaceAll(",", "")
          .replaceAll("\\.", "").split(" ")));
    }
    for (List<String> stringList : words) {
      for (String s : stringList) {
        int conunter = Collections.frequency(stringList, s);
        System.out.println(String.format(
            "Речення № " + (words.indexOf(stringList) + 1) + ", слово \"" + s + "\", к-сть повторень: "
                + conunter));
      }
    }
  }

  public static void task2(){
    String[] sentenses = getText().split("(\\.|\\?|\\!|[.]{3})(\\s)");
    for(int i = 0; i < sentenses.length; i++){
      for(int j = 0; j < sentenses.length - 1 - i; j++){
        if(sentenses[j].length() > sentenses[j + 1].length()){
          String str = sentenses[j];
          sentenses[j] = sentenses[j + 1];
          sentenses[j + 1] = str;
        }
      }
    }
    for(String s : sentenses){
      System.out.println(s);
    }
  }


  public static void main(String[] args) {
    System.out.println("\nTask1 - к-сть повторень слів у реченнях:");
    task1();
    System.out.println("\nTask2 - сортування речень за к-стю слів у них:");
    task2();
  }

}
